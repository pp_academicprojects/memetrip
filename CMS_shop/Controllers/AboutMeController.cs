﻿using CMS_shop.Data;
using CMS_shop.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CMS_shop.Controllers
{
    public class AboutMeController : Controller
    {
        private readonly ApplicationDbContext _dbContext;

        public AboutMeController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IActionResult Index()
        {
            return View(new ViewModel()
            {
                BlogInformationViewModel = _dbContext.BlogInformations.Take(1).ToList()
            }); ;
        }
    }
}
