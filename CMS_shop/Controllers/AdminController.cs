﻿using CMS_shop.Data;
using CMS_shop.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.CodeAnalysis.VisualBasic.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Hosting;
using System.Diagnostics;
using Page = CMS_shop.Models.Page;
using BlogInformation = CMS_shop.Models.BlogInformation;

namespace CMS_shop.Controllers
{
    //[Authorize(Roles = "Admin")]
    [Authorize]
    public class AdminController : Controller
    {
        private readonly ApplicationDbContext _dbContext;
        public AdminController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IActionResult Index()
        {
            var model = new ViewModel();

            return View(new ViewModel()
            {
                ContactMessagesViewModel = _dbContext.ContactMessages.Take(50).ToList(),
                BlogPostViewModel = _dbContext.BlogPosts.Take(50).ToList(),
                BlogInformationViewModel = _dbContext.BlogInformations.Take(1).ToList()
            });

        }
        //public IActionResult PostsList()
        //{
        //    return View(new ViewModel()
        //    {
        //        BlogPostViewModel = _dbContext.BlogPosts.Take(8).ToList(),
                
        //    }) ;
        //}

        #region PAGE
        public IActionResult EditPage(string title)
        {
            var page = _dbContext.Pages.FirstOrDefault(x => x.Title == title);

            if (page == null)
            {
                page = new Page();
                page.Title = title;
                _dbContext.Pages.Add(page);
                _dbContext.SaveChanges();
            }
            return View(page);
        }

        [HttpPost]
        public IActionResult SavePage(string title, string content)
        {
            var page = _dbContext.Pages.FirstOrDefault(x => x.Title == title);
            if (page == null)
            {
                return View("Error");
            }
            page.Content = content;
            _dbContext.SaveChanges();

            return RedirectToAction(nameof(Index));
        }
        #endregion

        public IActionResult ViewBlogPosts()
        {
            //ViewBag.Messagesa 
            try
            {

                var data = _dbContext.BlogPosts.Take(3).ToList();

                Debug.WriteLine("Data:");
                Debug.WriteLine(data);

                return View(data);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);

                return View("Error");
            }

        }

        #region BLOG POST
        
        public IActionResult AddEditBlogPost(int id)
        {
            
            var blogPost = _dbContext.BlogPosts.FirstOrDefault(x => x.Id == id);
            if (blogPost == null) // ADD NEW POST
            {
                blogPost = new BlogPost();
                blogPost.CreatedTime = DateTime.Now;
                blogPost.Id = id;
                _dbContext.BlogPosts.Add(blogPost);
                _dbContext.SaveChanges();
            }
            return View(blogPost);
        }

        [HttpPost]
        public IActionResult SaveBlogPost(int id, string header, string content, string category, string description)
        {
            var post = _dbContext.BlogPosts.FirstOrDefault(x => x.Id == id);
            if (post == null)
            {
                return View("Error");
            }
            post.Content = content;
            post.Header = header;
            post.Category = category;
            post.Description = description;
            _dbContext.SaveChanges();

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public IActionResult UploadImage(IFormFile image)
        {
            if (image == null || image.Length == 0)
                return BadRequest("No image provided");

            if (!image.ContentType.StartsWith("image"))
                return BadRequest("Invalid image type");

            if (image.Length > 10 * 1024 * 1024)
                return BadRequest("Image size too big");

            var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "images", image.FileName);
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                image.CopyTo(stream);
            }

            return Ok();
        }
        #endregion
        #region BLOG INFORMATIONS
        public IActionResult BlogInformation(string titleInformations)
        {
           titleInformations = "test";
            var blogInformation = _dbContext.BlogInformations.FirstOrDefault(x => x.TitleInformations == titleInformations);


            if (blogInformation == null)
            {
                blogInformation = new BlogInformation();
                blogInformation.TitleInformations = titleInformations;
                _dbContext.BlogInformations.Add(blogInformation);
                _dbContext.SaveChanges();
            }
            return View(blogInformation);
        }
        
        [HttpPost]
        public IActionResult SaveBlogInformations(string titleInformations, string privacyTitle, string privacyPolicy, string aboutMeTitle, string aboutMeDetails, string adminContactEmail,
            string adminContactAddress, string blogName, string footerCategory, string footerlinks, char backgroundColor)
        {

            
            
            var blogInformation = _dbContext.BlogInformations.FirstOrDefault(x => x.TitleInformations == titleInformations);
           
            if (blogInformation == null)
            {
                return View("Error");
            }
            blogInformation.AboutMeTitle = aboutMeTitle;
            blogInformation.AboutMeDetails = aboutMeDetails;
            blogInformation.PrivacyTitle = privacyTitle;
            blogInformation.PrivacyPolicy= privacyPolicy;
            blogInformation.AdminContactEmail= adminContactEmail;
            blogInformation.AdminContactAddress= adminContactAddress;
            blogInformation.BlogName= blogName;
            blogInformation.FooterCategory= footerCategory;
            blogInformation.Footerlinks= footerlinks;
            blogInformation.BackgroundColor= backgroundColor;
            _dbContext.SaveChanges();

            return RedirectToAction(nameof(Index));
        }
        //public IActionResult ViewBlogInformation()
        //{
        //    //ViewBag.Messagesa 
        //    try
        //    {

        //        var data = _dbContext.BlogInformations.Take(3).ToList();

        //        Debug.WriteLine("Data:");
        //        Debug.WriteLine(data);

        //        return View(data);
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex);

        //        return View("Error");
        //    }

        //}

        #endregion
        public IActionResult Message()
        {
            return View(new ViewModel()
            {
                ContactMessagesViewModel = _dbContext.ContactMessages.Take(50).ToList(),
            }); ;
        }
        public IActionResult MessageView()
        {
            return View(new ViewModel()
            {
                ContactMessagesViewModel = _dbContext.ContactMessages.Take(50).ToList(),
            }); ;
        }
    }
}
