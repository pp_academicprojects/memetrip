﻿using CMS_shop.Data;
using CMS_shop.Models;
using Microsoft.AspNetCore.Mvc;

namespace CMS_shop.Controllers
{
    public class CommentController : Controller
    {
        private readonly ApplicationDbContext _dbContext;

        public CommentController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IActionResult Index()
        {
            return View(new ViewModel()
            {
                CommentViewModek = _dbContext.Comments.Take(50).ToList()
            }); ;

        }
    }
}
