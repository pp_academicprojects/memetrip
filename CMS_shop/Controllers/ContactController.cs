﻿using CMS_shop.Data;
using CMS_shop.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;

namespace CMS_shop.Controllers
{
    public class ContactController : Controller
    {
        private readonly ApplicationDbContext _dbContext;

        public ContactController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IActionResult Index(int messageId)
        {
            
            return View(new ViewModel()
            {
                ContactMessagesViewModel = _dbContext.ContactMessages.Take(50).ToList(),
                BlogInformationViewModel = _dbContext.BlogInformations.Take(1).ToList(),
            }); ;
        }



        [HttpPost]
        public IActionResult SaveContactMessage(int messageId, string userEmail, string userName, string userMessage)
        {
            //BlogPost? blogPost;
            //var contactMessage = _dbContext.ContactMessages.FirstOrDefault(x => x.MessageId == messageId);

            //if (contactMessage == null)
            //{
            //    return View("Error");
            //}
            var contactMessage = _dbContext.ContactMessages.FirstOrDefault(x => x.MessageId == messageId);
            if (contactMessage == null)
            {
                return View("Error");
            }
            contactMessage.UserMessage = userMessage;
            contactMessage.UserName = userName;
            contactMessage.UserEmail = userEmail;
                _dbContext.SaveChanges();

            return RedirectToAction(nameof(Index));
        }

        public IActionResult UserContactMessage(int messageId)
        {
            var contactMessage = _dbContext.ContactMessages.FirstOrDefault(x => x.MessageId == messageId);

            if (contactMessage == null) // ADD NEW POST
            {
                contactMessage = new ContactMessage();
                contactMessage.MessageDate = DateTime.Now;
                contactMessage.MessageId = messageId;
                _dbContext.ContactMessages.Add(contactMessage);
                _dbContext.SaveChanges();
            }
            

            return View(contactMessage);
        }
        public IActionResult DisplayMessage()
        {
            return View(new ViewModel()
            {
                ContactMessagesViewModel = _dbContext.ContactMessages.Take(50).ToList(),
            }); ;
        }
    }
}
