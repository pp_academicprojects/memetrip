﻿using CMS_shop.Data;
using CMS_shop.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace CMS_shop.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _dbContext;
        public HomeController(ApplicationDbContext dbContext)
        {
            _dbContext=dbContext;
        }
        //private readonly ILogger<HomeController> _logger;

        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}

        public IActionResult Index()
        {
            return View(new ViewModel()
            {
                BlogPostViewModel = _dbContext.BlogPosts.Take(50).ToList(),
                BlogInformationViewModel = _dbContext.BlogInformations.Take(1).ToList()
            }); ;
        }

        public IActionResult Privacy()
        {
            var model = _dbContext.BlogInformations.FirstOrDefault();
            return View(model);


            //return View(new ViewModel()
            //{
            //    BlogInformationViewModel = _dbContext.BlogInformations.Take(1).ToList()
            //});;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}