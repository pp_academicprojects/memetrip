﻿using CMS_shop.Data;
using CMS_shop.Models;
using Microsoft.AspNetCore.Mvc;

namespace CMS_shop.Controllers
{
    public class PostsController : Controller
    {
        private readonly ApplicationDbContext _dbContext;

        public PostsController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IActionResult Index(int id)
        {
            var blogPost = _dbContext.BlogPosts.FirstOrDefault(x => x.Id == id);
            var comments = _dbContext.Comments.Where(q => q.BlogPostId == blogPost.Id).ToList();
            return View(new BlogPostViewModel
            {
                BlogPost = blogPost,
                Comments = comments
            });
        }
            //    return View(new ViewModel()
            //    {

            //        BlogPostViewModel = _dbContext.BlogPosts.Take(8).ToList(),
            //        CommentViewModek = _dbContext.Comments.Take(10).ToList()
            //    }); 
            //}

        
        public IActionResult Comment(int blogId)
        {
            var comment = new Comment();
            comment.CommentDate = DateTime.Now;
            //comment.CommentsId = commentsId;
            comment.BlogPostId = blogId;
            _dbContext.Comments.Add(comment);
            _dbContext.SaveChanges();

            return View(comment);
        }

        

        [HttpPost]
        public IActionResult SaveComment(int commentId, string userName, string userComment, int blogId)
        {
            var comment = _dbContext.Comments.FirstOrDefault(x => x.CommentsId == commentId);
            //var post = _dbContext.BlogPosts.FirstOrDefault(x => x.Id == id);
            if (comment == null)
            {
                return View("Error");
            }
            comment.UserName = userName;
            comment.UserComment = userComment;
            _dbContext.SaveChanges();
           

            return RedirectToAction(nameof(Index), new { id = blogId });
        }
    }
}
