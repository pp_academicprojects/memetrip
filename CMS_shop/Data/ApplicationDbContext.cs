﻿using CMS_shop.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CMS_shop.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Page> Pages { get; set; }

        public DbSet<BlogPost> BlogPosts { get; set; }
        public DbSet<BlogInformation> BlogInformations { get; set; }
        public DbSet<ContactMessage> ContactMessages { get; set; }
        public DbSet<Comment>Comments { get; set; }
        
    }
}