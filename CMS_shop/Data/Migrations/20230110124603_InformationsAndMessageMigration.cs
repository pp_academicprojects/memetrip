﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CMS_shop.Data.Migrations
{
    public partial class InformationsAndMessageMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "BlogPosts",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BlogInformations",
                columns: table => new
                {
                    TitleInformations = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    PrivacyTitle = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PrivacyPolicy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AboutMeTitle = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AboutMeDetails = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AdminContactEmail = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AdminContactAddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BlogName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FooterCategory = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Footerlinks = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BackgroundColor = table.Column<string>(type: "nvarchar(1)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogInformations", x => x.TitleInformations);
                });

            migrationBuilder.CreateTable(
                name: "ContactMessages",
                columns: table => new
                {
                    MessageId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserEmail = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserMessage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MessageDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactMessages", x => x.MessageId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BlogInformations");

            migrationBuilder.DropTable(
                name: "ContactMessages");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "BlogPosts");
        }
    }
}
