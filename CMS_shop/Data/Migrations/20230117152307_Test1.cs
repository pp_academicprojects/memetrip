﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CMS_shop.Data.Migrations
{
    public partial class Test1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BlogInformationTitleInformations",
                table: "ContactMessages",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BlogPostId",
                table: "ContactMessages",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BlogPostId",
                table: "BlogPosts",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BlogInformationTitleInformations",
                table: "BlogInformations",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BlogInformationBlogPost",
                columns: table => new
                {
                    BlogInformationViewModelTitleInformations = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    BlogPostViewModelId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogInformationBlogPost", x => new { x.BlogInformationViewModelTitleInformations, x.BlogPostViewModelId });
                    table.ForeignKey(
                        name: "FK_BlogInformationBlogPost_BlogInformations_BlogInformationViewModelTitleInformations",
                        column: x => x.BlogInformationViewModelTitleInformations,
                        principalTable: "BlogInformations",
                        principalColumn: "TitleInformations",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BlogInformationBlogPost_BlogPosts_BlogPostViewModelId",
                        column: x => x.BlogPostViewModelId,
                        principalTable: "BlogPosts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ContactMessages_BlogInformationTitleInformations",
                table: "ContactMessages",
                column: "BlogInformationTitleInformations");

            migrationBuilder.CreateIndex(
                name: "IX_ContactMessages_BlogPostId",
                table: "ContactMessages",
                column: "BlogPostId");

            migrationBuilder.CreateIndex(
                name: "IX_BlogPosts_BlogPostId",
                table: "BlogPosts",
                column: "BlogPostId");

            migrationBuilder.CreateIndex(
                name: "IX_BlogInformations_BlogInformationTitleInformations",
                table: "BlogInformations",
                column: "BlogInformationTitleInformations");

            migrationBuilder.CreateIndex(
                name: "IX_BlogInformationBlogPost_BlogPostViewModelId",
                table: "BlogInformationBlogPost",
                column: "BlogPostViewModelId");

            migrationBuilder.AddForeignKey(
                name: "FK_BlogInformations_BlogInformations_BlogInformationTitleInformations",
                table: "BlogInformations",
                column: "BlogInformationTitleInformations",
                principalTable: "BlogInformations",
                principalColumn: "TitleInformations");

            migrationBuilder.AddForeignKey(
                name: "FK_BlogPosts_BlogPosts_BlogPostId",
                table: "BlogPosts",
                column: "BlogPostId",
                principalTable: "BlogPosts",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ContactMessages_BlogInformations_BlogInformationTitleInformations",
                table: "ContactMessages",
                column: "BlogInformationTitleInformations",
                principalTable: "BlogInformations",
                principalColumn: "TitleInformations");

            migrationBuilder.AddForeignKey(
                name: "FK_ContactMessages_BlogPosts_BlogPostId",
                table: "ContactMessages",
                column: "BlogPostId",
                principalTable: "BlogPosts",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BlogInformations_BlogInformations_BlogInformationTitleInformations",
                table: "BlogInformations");

            migrationBuilder.DropForeignKey(
                name: "FK_BlogPosts_BlogPosts_BlogPostId",
                table: "BlogPosts");

            migrationBuilder.DropForeignKey(
                name: "FK_ContactMessages_BlogInformations_BlogInformationTitleInformations",
                table: "ContactMessages");

            migrationBuilder.DropForeignKey(
                name: "FK_ContactMessages_BlogPosts_BlogPostId",
                table: "ContactMessages");

            migrationBuilder.DropTable(
                name: "BlogInformationBlogPost");

            migrationBuilder.DropIndex(
                name: "IX_ContactMessages_BlogInformationTitleInformations",
                table: "ContactMessages");

            migrationBuilder.DropIndex(
                name: "IX_ContactMessages_BlogPostId",
                table: "ContactMessages");

            migrationBuilder.DropIndex(
                name: "IX_BlogPosts_BlogPostId",
                table: "BlogPosts");

            migrationBuilder.DropIndex(
                name: "IX_BlogInformations_BlogInformationTitleInformations",
                table: "BlogInformations");

            migrationBuilder.DropColumn(
                name: "BlogInformationTitleInformations",
                table: "ContactMessages");

            migrationBuilder.DropColumn(
                name: "BlogPostId",
                table: "ContactMessages");

            migrationBuilder.DropColumn(
                name: "BlogPostId",
                table: "BlogPosts");

            migrationBuilder.DropColumn(
                name: "BlogInformationTitleInformations",
                table: "BlogInformations");
        }
    }
}
