﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CMS_shop.Data.Migrations
{
    public partial class Test2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContactMessages_BlogInformations_BlogInformationTitleInformations",
                table: "ContactMessages");

            migrationBuilder.DropForeignKey(
                name: "FK_ContactMessages_BlogPosts_BlogPostId",
                table: "ContactMessages");

            migrationBuilder.DropIndex(
                name: "IX_ContactMessages_BlogInformationTitleInformations",
                table: "ContactMessages");

            migrationBuilder.DropColumn(
                name: "BlogInformationTitleInformations",
                table: "ContactMessages");

            migrationBuilder.RenameColumn(
                name: "BlogPostId",
                table: "ContactMessages",
                newName: "ContactMessageMessageId");

            migrationBuilder.RenameIndex(
                name: "IX_ContactMessages_BlogPostId",
                table: "ContactMessages",
                newName: "IX_ContactMessages_ContactMessageMessageId");

            migrationBuilder.CreateTable(
                name: "BlogInformationContactMessage",
                columns: table => new
                {
                    BlogInformationViewModelTitleInformations = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ContactMessagesViewModelMessageId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogInformationContactMessage", x => new { x.BlogInformationViewModelTitleInformations, x.ContactMessagesViewModelMessageId });
                    table.ForeignKey(
                        name: "FK_BlogInformationContactMessage_BlogInformations_BlogInformationViewModelTitleInformations",
                        column: x => x.BlogInformationViewModelTitleInformations,
                        principalTable: "BlogInformations",
                        principalColumn: "TitleInformations",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BlogInformationContactMessage_ContactMessages_ContactMessagesViewModelMessageId",
                        column: x => x.ContactMessagesViewModelMessageId,
                        principalTable: "ContactMessages",
                        principalColumn: "MessageId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BlogPostContactMessage",
                columns: table => new
                {
                    BlogPostViewModelId = table.Column<int>(type: "int", nullable: false),
                    ContactMessagesViewModelMessageId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogPostContactMessage", x => new { x.BlogPostViewModelId, x.ContactMessagesViewModelMessageId });
                    table.ForeignKey(
                        name: "FK_BlogPostContactMessage_BlogPosts_BlogPostViewModelId",
                        column: x => x.BlogPostViewModelId,
                        principalTable: "BlogPosts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BlogPostContactMessage_ContactMessages_ContactMessagesViewModelMessageId",
                        column: x => x.ContactMessagesViewModelMessageId,
                        principalTable: "ContactMessages",
                        principalColumn: "MessageId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    CommentsId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserComment = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CommentDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BlogPostId = table.Column<int>(type: "int", nullable: false),
                    BlogInformationTitleInformations = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    ContactMessageMessageId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.CommentsId);
                    table.ForeignKey(
                        name: "FK_Comments_BlogInformations_BlogInformationTitleInformations",
                        column: x => x.BlogInformationTitleInformations,
                        principalTable: "BlogInformations",
                        principalColumn: "TitleInformations");
                    table.ForeignKey(
                        name: "FK_Comments_BlogPosts_BlogPostId",
                        column: x => x.BlogPostId,
                        principalTable: "BlogPosts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Comments_ContactMessages_ContactMessageMessageId",
                        column: x => x.ContactMessageMessageId,
                        principalTable: "ContactMessages",
                        principalColumn: "MessageId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_BlogInformationContactMessage_ContactMessagesViewModelMessageId",
                table: "BlogInformationContactMessage",
                column: "ContactMessagesViewModelMessageId");

            migrationBuilder.CreateIndex(
                name: "IX_BlogPostContactMessage_ContactMessagesViewModelMessageId",
                table: "BlogPostContactMessage",
                column: "ContactMessagesViewModelMessageId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_BlogInformationTitleInformations",
                table: "Comments",
                column: "BlogInformationTitleInformations");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_BlogPostId",
                table: "Comments",
                column: "BlogPostId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_ContactMessageMessageId",
                table: "Comments",
                column: "ContactMessageMessageId");

            migrationBuilder.AddForeignKey(
                name: "FK_ContactMessages_ContactMessages_ContactMessageMessageId",
                table: "ContactMessages",
                column: "ContactMessageMessageId",
                principalTable: "ContactMessages",
                principalColumn: "MessageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContactMessages_ContactMessages_ContactMessageMessageId",
                table: "ContactMessages");

            migrationBuilder.DropTable(
                name: "BlogInformationContactMessage");

            migrationBuilder.DropTable(
                name: "BlogPostContactMessage");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.RenameColumn(
                name: "ContactMessageMessageId",
                table: "ContactMessages",
                newName: "BlogPostId");

            migrationBuilder.RenameIndex(
                name: "IX_ContactMessages_ContactMessageMessageId",
                table: "ContactMessages",
                newName: "IX_ContactMessages_BlogPostId");

            migrationBuilder.AddColumn<string>(
                name: "BlogInformationTitleInformations",
                table: "ContactMessages",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ContactMessages_BlogInformationTitleInformations",
                table: "ContactMessages",
                column: "BlogInformationTitleInformations");

            migrationBuilder.AddForeignKey(
                name: "FK_ContactMessages_BlogInformations_BlogInformationTitleInformations",
                table: "ContactMessages",
                column: "BlogInformationTitleInformations",
                principalTable: "BlogInformations",
                principalColumn: "TitleInformations");

            migrationBuilder.AddForeignKey(
                name: "FK_ContactMessages_BlogPosts_BlogPostId",
                table: "ContactMessages",
                column: "BlogPostId",
                principalTable: "BlogPosts",
                principalColumn: "Id");
        }
    }
}
