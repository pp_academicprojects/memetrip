﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CMS_shop.Data.Migrations
{
    public partial class BlogMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SmallPhotoPath",
                table: "BlogPosts",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SmallPhotoPath",
                table: "BlogPosts");
        }
    }
}
