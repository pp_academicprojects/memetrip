﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;

namespace CMS_shop.Models
{
    
    public class BlogInformation:ViewModel
    {
        [Key]
        public string? TitleInformations { get; set; }
        public string? PrivacyTitle { get; set; }
        public string? PrivacyPolicy { get; set; }
        public string? AboutMeTitle { get; set; }
        public string? AboutMeDetails { get; set; }
        [NotMapped]
        public Image? AboutMeImg { get; set; }
        [NotMapped]
        public Image? BackgroundImg { get; set; }
        public string? AdminContactEmail { get; set; }
        public string? AdminContactAddress { get; set; }
        public string? BlogName { get; set; }
        public string? FooterCategory { get; set; }
        public string? Footerlinks { get; set; }
        public char? BackgroundColor { get; set; }
       

    }
}
