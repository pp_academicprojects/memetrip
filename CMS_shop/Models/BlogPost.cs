﻿using Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;

namespace CMS_shop.Models
{
    public class BlogPost:ViewModel
    {
        [Key]
        public int Id { get; set; }

        public DateTime CreatedTime { get; set; }

        public DateTime? LastEditTime { get; set; }

        public string? Category { get; set; }


        public string? Header { get; set; }


        public string? Content { get; set; }


        //public List<string>? Hashtags { get; set; }


        public int ViewsCounter { get; set; }

        [NotMapped]
        public Image? PhotoSmall { get; set; }


        [NotMapped]
        public Image? PhotoBig { get; set; }

        public string? SmallPhotoPath { get; set; }

        //public string? PhotoSmall_1 { get; set; }

        //public IFormFile? PhotoBig_1 { get; set; }

        public string? Description { get; set; }


        // FUTURE: ADD USER COMMENTS
    }
}
