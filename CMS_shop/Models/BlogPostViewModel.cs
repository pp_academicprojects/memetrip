﻿namespace CMS_shop.Models
{
    public class BlogPostViewModel
    {
        public BlogPost BlogPost { get; set; }

        public List<Comment> Comments { get; set; } = new List<Comment>();
    }
}
