﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS_shop.Models
{
    public class Comment
    {
        [Key]
        public int CommentsId { get; set; }
        public string? UserComment { get; set; }
        public DateTime CommentDate { get; set; }
        public string? UserName { get; set; }
        public int BlogPostId { get; set; }
        [ForeignKey("BlogPostId")]
        public virtual BlogPost? BlogPost { get; set; }
    }
}
