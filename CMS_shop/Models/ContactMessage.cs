﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS_shop.Models
{
    public class ContactMessage:ViewModel
    {
        [Key]
        public int MessageId { get; set; }
        public string? UserEmail { get; set; }
        public string? UserName { get; set; }
        public string? UserMessage { get; set; }
        public DateTime MessageDate { get; set; }
        
      
    }
}
