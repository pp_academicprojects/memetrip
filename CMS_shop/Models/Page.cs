﻿using System.ComponentModel.DataAnnotations;

namespace CMS_shop.Models
{
    public class Page
    {
        [Key]
        public string? Title { get; set; }
        public string? Content { get; set; }

    }
}
