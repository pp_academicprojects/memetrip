﻿using Microsoft.EntityFrameworkCore;

namespace CMS_shop.Models
{
    
    public class ViewModel 
    {
        public List<BlogPost>? BlogPostViewModel { get; set; }
        public IEnumerable<BlogInformation>? BlogInformationViewModel { get; set; }
        public IEnumerable<ContactMessage>? ContactMessagesViewModel { get; set; }
        public IEnumerable<Comment>? CommentViewModek { get; set; }
    }
}
